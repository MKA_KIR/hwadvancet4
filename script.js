class taskColumn {
    constructor({columnClass, columnTitleClass, columnTitleText, columnTasksListClass, addButtonClass, addFormClass, taskClass, sortButtonClass}) {
        this.columnClass = columnClass;
        this.columnTitleClass = columnTitleClass;
        this.columnTitleText = columnTitleText;        
        this.columnTasksListClass = columnTasksListClass;
        this.addButtonClass = addButtonClass;  
        this.addFormClass = addFormClass;
        this.taskClass = taskClass;
        this.sortButtonClass = sortButtonClass;
        this.column = null;
        this.dragElem = null;
    }
    render() {
        this.column = document.createElement('div');
        this.column.className = this.columnClass;

        this.column.insertAdjacentHTML("beforeend", `<h2 class="${this.columnTitleClass}">${this.columnTitleText}</h2>
                                                    <ul class="${this.columnTasksListClass}"></ul>`);
        const tasksList = this.column.querySelector(`.${this.columnTasksListClass}`);
        tasksList.addEventListener('dragover', this.allowDrop);
        tasksList.addEventListener('drop', this.drop.bind(this));        
        const buttonCreate = this.createAddButton();
        const buttonSort = document.createElement('button');
        buttonSort.textContent = 'sort';
        buttonSort.className = this.sortButtonClass;
        buttonSort.addEventListener('click', this.sortCards.bind(this));
        this.column.append(buttonSort, buttonCreate);
        return this.column;
    }
    
    handleCreateCard() {
        const formCreateCard = document.createElement('form');
        formCreateCard.className = this.addFormClass;
        formCreateCard.insertAdjacentHTML('beforeend', `<div class="form-group"><textarea placeholder="Введите описание карточки"></textarea></div><div class="form-group"><input type="submit" value="Создать карточку"><span class="cancel-create-card">&#10006;</span></div>`);
        
        formCreateCard.addEventListener("submit", this.createCard.bind(this));
        formCreateCard.addEventListener("click", ({target}) => {
            if (target.classList.contains("cancel-create-card")) {
                target.closest("form").remove();
                const buttonCreate = this.createAddButton();
                this.column.append(buttonCreate);
            }
        });
        
        this.column.append(formCreateCard);
        this.column.querySelector(`.${this.addButtonClass}`).remove();
    }

    createAddButton() {
        const buttonCreate = document.createElement('button');
        buttonCreate.textContent = 'Create task';
        buttonCreate.className = this.addButtonClass;
        buttonCreate.addEventListener('click', this.handleCreateCard.bind(this));
        return buttonCreate;
    }

    createCard(e) {
        e.preventDefault();
        const field = e.target.querySelector('textarea');
        const taskText = field.value;
        if (taskText) {
            const task = document.createElement('div');
            task.addEventListener('dragstart', this.dragStart.bind(this));
            task.draggable = true;
            task.className = this.taskClass;
            task.insertAdjacentHTML('beforeend', `<p style="color:black">${taskText}</p><span class="delete-card">&times;</span>`);
            task.addEventListener('click', this.deleteCard);
            const tasksList = this.column.querySelector(`.${this.columnTasksListClass}`);
            tasksList.append(task);
            e.target.remove();
            const buttonCreate = this.createAddButton();
            this.column.append(buttonCreate);
        } else {
            e.target.insertAdjacentHTML('beforeend', `<p style="color:red">Введите название карточки</p>`)
        }
    }
    
    deleteCard(e) {
        if (e.target.classList.contains('delete-card')) {
            this.remove()
        }
    }
    
    allowDrop(e) {
        e.preventDefault();
    }
    
    drop(e) {
        e.preventDefault();
        console.log(e.target)
        let {target} = e;
        if(!e.target.classList.contains(this.taskClass)) {
            target = target.closest(`.${this.taskClass}`);
        }
        if(!target){
            let {clientX, clientY} = e;
            clientY += 5;
            let elementMouseIsOver = document.elementFromPoint(clientX, clientY);
            while(!elementMouseIsOver.classList.contains(this.taskClass)) {
                clientY += 5;
                elementMouseIsOver = document.elementFromPoint(clientX, clientY);  
            }
            target = elementMouseIsOver;
        }
        target.before(this.dragElem);
        this.dragElem = null;
    }
    
    dragStart(e) {
        this.dragElem = e.target;
    }
    
    sortCards() {
        const allCards = [...this.column.querySelectorAll(`.${this.taskClass}`)];
        const sortCards = allCards.sort(function (card1, card2) {
            const card1Text = card1.querySelector('p').textContent;
            const card2Text = card2.querySelector('p').textContent;
            if (card1Text < card2Text) {
                return -1;
            }
            if (card1Text > card2Text) {
                return 1;
            }
            return 0;
        });
        allCards.forEach(card => card.remove());
        const tasksList = this.column.querySelector(`.${this.columnTasksListClass}`);
        tasksList.append(...sortCards);
    }
}
const trelloColumnProps = {
    columnClass: 'task-column',
    columnTitleClass: 'task-column-title',
    columnTitleText: 'Tasks to Do',
    columnTasksListClass: 'task-column-items',
    addButtonClass: 'add-card-btn',
    addFormClass: 'form-add-card',
    taskClass: 'task-card',
    sortButtonClass: 'sort-btn'
};
const columnToDo = new taskColumn(trelloColumnProps);
const taskBoard = document.getElementById('task-board');
taskBoard.append(columnToDo.render());
